package com.thyu.entity;

import lombok.Data;

@Data
public class User {
    private int id;
    private String name;
    private int age;
    private String address;
    //private String schoolId;
    private Sex sex;
}

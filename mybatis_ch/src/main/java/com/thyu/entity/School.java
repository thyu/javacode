package com.thyu.entity;

import lombok.Data;

@Data
public class School {
    private String name;
    private String address;
    private String schoolId;

}

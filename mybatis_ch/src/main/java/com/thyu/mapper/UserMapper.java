package com.thyu.mapper;

import com.thyu.entity.User;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.cache.decorators.FifoCache;

import java.util.List;

/**
 * <cache eviction="FIFO"
 * flushInterval="60000"
 * size="512"
 * readOnly="true"/>
 * <p>
 * 二级缓存容易出现脏数据
 * 频繁刷新缓存(一旦出现update,insert,delete全部失效)
 * 使用redis，memcache等代替
 */
@Mapper
@CacheNamespace(eviction = FifoCache.class, size = 512, flushInterval = 6000)
public interface UserMapper {
    List<User> selectAll();

    @Select("select * from user where id=#{id}")
    User selectById(@Param("id") int id);

    List<User> selectByLikeName(@Param("name") String name);

    @Insert("insert into user values(null,#{name},#{age},#{address},#{sex})")
    void insertUser(User user);

    List<User> selectWithPage(@Param("start") int start, @Param("end") int end);
}

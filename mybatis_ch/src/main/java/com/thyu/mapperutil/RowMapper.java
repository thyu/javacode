package com.thyu.mapperutil;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface RowMapper<T> {
    T mapper(ResultSet resultSet) throws SQLException;
}

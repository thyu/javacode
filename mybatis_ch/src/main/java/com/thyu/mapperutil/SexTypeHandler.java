package com.thyu.mapperutil;

import com.thyu.entity.Sex;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedJdbcTypes(JdbcType.INTEGER)
public class SexTypeHandler extends BaseTypeHandler<Sex> {
    private static final Logger LOGGER = Logger.getLogger(SexTypeHandler.class);

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Sex parameter, JdbcType jdbcType) throws SQLException {
        LOGGER.info("use this sex handler......................");
        ps.setInt(i, this.changeSexToInt(parameter));
    }

    @Override
    public Sex getNullableResult(ResultSet rs, String columnName) throws SQLException {
        int rsInt = rs.getInt(columnName);
        return this.changeNumToSex(rsInt);
    }

    @Override
    public Sex getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        return null;
    }

    @Override
    public Sex getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        return null;
    }


    private int changeSexToInt(Sex sex) {
        switch (sex) {
            case MAN:
                return 1;
            case WOMAN:
                return 0;
            default:
                throw new RuntimeException("Error");
        }
    }

    private Sex changeNumToSex(int i) {
        switch (i) {
            case 0:
                return Sex.WOMAN;
            case 1:
                return Sex.MAN;
            default:
                throw new RuntimeException("Error");
        }
    }
}

package com.thyu.ch1;

import cn.hutool.core.util.RandomUtil;
import com.thyu.entity.Sex;
import com.thyu.entity.User;
import com.thyu.mapper.UserMapper;
import com.thyu.mapperutil.RowMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Objects;

/**
 * @author thyu
 */
public class Demo_1 {
    private static SqlSessionFactory factory;

    @BeforeAll
    public static void init() {
        String resources = "mybatis_config.xml";
        try (InputStream inputStream = Resources.getResourceAsStream(resources)) {
            factory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void initSqlSessionFactory() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            Connection connection = session.getConnection();
            PreparedStatement tables = connection.prepareStatement("select  * from user ");
            ResultSet resultSet = tables.executeQuery();
            RowMapper<User> userRowMapper = r -> {
                User user = new User();
                user.setId(r.getInt("id"));
                user.setName(r.getString("name"));
                user.setAge(r.getInt("age"));
                user.setAddress(r.getString("address"));
                return user;
            };
            while (resultSet.next()) {
                System.out.println(userRowMapper.mapper(resultSet));
            }
            connection.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }

    @Test
    public void testPlugin() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            List<User> users = mapper.selectAll();
            users.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }


    @Test
    public void testAnnotation() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            User user = mapper.selectById(1);
            User user2 = mapper.selectById(1);

            System.out.println(user);
            System.out.println("---------------------------------");
            System.out.println(user2);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }

    @Test
    public void testdynamicSql() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            List<User> users = mapper.selectByLikeName(null);
            users.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }

    @Test
    public void testSecondCache() {
        SqlSession session = null;
        SqlSession session2 = null;
        try {
            session = factory.openSession();
            session2 = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            UserMapper mapper2 = session2.getMapper(UserMapper.class);
            User user = mapper.selectById(1);
            User user2 = mapper2.selectById(1);
            System.out.println("-------------------------------------------------");

            List<User> users = mapper.selectAll();
            List<User> users2 = mapper2.selectAll();

        } catch (Exception e) {
            if (Objects.nonNull(session)) {
                session.close();
            }
            if (Objects.nonNull(session2)) {
                session2.close();
            }
        }
    }

    @Test
    public void testInsert() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            User user = new User();
            user.setId(5);
            user.setAddress("重庆");
            user.setAge(20);
            user.setName("李华");
            user.setSex(Sex.MAN);
            mapper.insertUser(user);
            session.commit();
        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }

    @Test
    public void insertMany() {
        SqlSession session = null;
        try {
            session = factory.openSession(ExecutorType.BATCH, false);
            UserMapper mapper = session.getMapper(UserMapper.class);
            for (int i = 0; i < 1000; i++) {
                User user = new User();
                user.setAddress(RandomUtil.randomString(10));
                user.setAge(RandomUtil.randomInt(30));
                user.setName(RandomUtil.randomString(8));
                int temp = RandomUtil.randomInt(10);
                if (temp > 5) {
                    user.setSex(Sex.MAN);
                } else {
                    user.setSex(Sex.WOMAN);
                }
                mapper.insertUser(user);
                if (i % 100 == 0) {
                    session.commit();
                }
            }
            session.commit();

        } catch (Exception e) {
            session.rollback();
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }

    }

    @Test
    public void testPage() {
        SqlSession session = null;
        try {
            session = factory.openSession();
            UserMapper mapper = session.getMapper(UserMapper.class);
            List<User> users = mapper.selectWithPage(80, 1);
            users.forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(session)) {
                session.close();
            }
        }
    }


    @AfterAll
    public static void end() {

    }


}
